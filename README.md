# gitlab_checks

Security audit tool to perform common security checks on a GitLab repository. 

These checks are provided to support a security audit of a GitLab.com SaaS instance.

![Example command line output of gitlab_checks](/example.png "Example output")

Current checks:

| Severity | Check                                                                               | Conditions |
|----------|-------------------------------------------------------------------------------------|------------|
| HIGH | Root group does not have MFA enabled                                                |  |
| MEDIUM | Sharing with groups outside the root group is allowed                               | Non public groups | 
| MEDIUM | Billable members are present in the group that have not logged in for 60 days       | |
| MEDIUM | Billable members are present in the group that have no activity in the past 90 days |  |
| LOW | Root group allows users to fork repositories to outside groups                      | Non public groups | 
| LOW | Root group does not have default branch protection set to protect the main/default branch | |
| INFORMATIONAL | Root group visibility is set to public                                              | |


## Installation

Install the application directly from the RubyGems repository:

    $ gem install gitlab_checks

### Mac Installation

For Mac users, the default version of Ruby does not allow gems to be installed without administrator access. 

Use Ruby installed using MacPorts or Homebrew, or install the gem locally:

    $ gem install --user-install gitlab_checks
    $ echo 'export PATH="~/.gem/ruby/2.6.0/bin:$PATH"' >> ~/.zshrc

Note: Monetery and Ventura currently use Ruby 2.6.0 for others you may need to change the version number

## Usage

A GitLab [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) or OAuth2 access token with read API permissions is required to access the data needed to perform the checks

Provide Gitlab API endpoint, Gitlab token and Gitlab organisation id:

    $ Usage: gitlab_checks --url URL --group GROUPID --token TOKEN
    -u, --url URL                    Url of Gitlab API
    -g, --group GROUPID              ID of the root group for processing
    -t, --token TOKEN                GitLab private token or an OAuth2 access token
    -h, --help                       Display this screen

The GitLab token can also be provided in the environment variable GITLAB_API_PRIVATE_TOKEN

### Examples 

With environment variable 

    $ export GITLAB_API_PRIVATE_TOKEN=glpat-....
    $ gitlab_checks --url https://gitlab.com/api/v4 --group 12345678

Without environment variable

    $ gitlab_checks --url https://gitlab.com/api/v4 --group 12345678 --token glpat-xxx

## Development

Download the repo:

    $ git clone git@gitlab.com:zerosource/tools/gitlab_checks.git

Run directly from source:

    $ bundle install
    $ ruby bin/gitlab_checks --url https://gitlab.com/api/v4 --group 12345678 --token glpat-xxx

Build the gem package for installation into Ruby:

    $ gem build GitlabChecks.gemspec

Install the gem by executing:

    $ gem install gitlab_checks

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/zerosource/tools/gitlab_checks. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/zerosource/tools/gitlab_checks/-/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [Apache-2.0 License](https://www.apache.org/licenses/LICENSE-2.0).

## Code of Conduct

Everyone interacting in the gitlab_checks project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/zerosource/tools/gitlab_checks/-/blob/main/CODE_OF_CONDUCT.md).

## Feedback

Zerosource is committed to supporting developers improve their security through open source tools and information. 

Reach out to us with feedback via email:

    community AT zerosource DOT io
