# frozen_string_literal: true

require_relative "lib/gitlab_checks/version"

Gem::Specification.new do |spec|
  spec.name = "gitlab_checks"
  spec.version = GitlabChecks::VERSION
  spec.authors = "Zerosource"
  spec.email = "hello@zerosource.io"

  spec.summary = "Security checks for GitLab organisations and repositories"
  spec.description = "Perform a number of common security checks against your GitLab organisation"
  spec.homepage = "https://zerosource.io"
  spec.license = "Apache-2.0"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/zerosource/tools/gitlab_checks"
  spec.metadata["changelog_uri"] = "https://gitlab.com/zerosource/tools/CHANGELOG.md"
  spec.metadata["bug_tracker_uri"] = "https://gitlab.com/zerosource/tools/gitlab_checks/issues"

    # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|circleci)|appveyor)})
    end
  end

  spec.require_paths = ["lib"]
  spec.executables = ["gitlab_checks"]

  spec.add_dependency "gitlab", "~> 4.19.0"
  spec.add_dependency "optparse", "~> 0.3.1"

end
