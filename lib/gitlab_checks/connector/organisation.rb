# frozen_string_literal: true

require "gitlab"

module GitlabChecks
  module Connector
    class Organisation
      def initialize(_endpoint, _token, _group)
        @root_group = nil
        @subgroups = nil
        @member = nil
        @repositories = nil

        Gitlab.configure do |config|
          config.endpoint = _endpoint
          config.private_token = _token if !_token.nil?
          config.user_agent = "Gitlab Checks v#{GitlabChecks::VERSION} (https://gitlab.com/zerosource/tools/gitlab_checks)"
        end

        @root_group = _group
      end

      attr_reader :root_group
    end
  end
end
