# frozen_string_literal: true

module GitlabChecks
  module CLI
  end
end

require "gitlab_checks/cli/application"
