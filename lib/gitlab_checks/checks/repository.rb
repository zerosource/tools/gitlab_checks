# frozen_string_literal: true

module GitlabChecks
  module Checks
    class Repository
      def initialize
        @all_repos = nil
        @repos_with_gitlab_security = nil
      end

      def audit(gitlab_org)
        findings = []

        @all_repos = Gitlab.group_projects(gitlab_org.root_group,
                                           { per_page: 100, include_subgroups: true }).lazy_paginate

        @repos_with_gitlab_security = Gitlab.group_projects(gitlab_org.root_group,
                                                            { per_page: 100, include_subgroups: true,
                                                              with_security_reports: true }).lazy_paginate

        findings
      end

      def output_result; end

      def output_statistics
        print "Number of projects: #{@all_repos.count}"
        print "Number of projects with active Gitlab security dashboards: #{@repos_with_gitlab_security.count}"
        print "Number of public projects: #{ @all_repos.select { |repo| repo["visibility"].include?("public") }.count}"
        print "Number of internal projects: #{ @all_repos.select { |repo| repo["visibility"].include?("internal") }.count}"
      end

      private

      def print(message)
        $stdout.puts message
      end
    end
  end
end
