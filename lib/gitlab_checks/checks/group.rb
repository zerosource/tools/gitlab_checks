# frozen_string_literal: true

module GitlabChecks
  module Checks
    class Group
      def audit(gitlab_org)
        findings = []

        @group = Gitlab.group(gitlab_org.root_group)

        if !!@group["require_two_factor_authentication"] != true
          findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:HIGH],
                                                          "Two-factor authentication not required for group #{@group.name}", "To reduce the risk of unauthorised access, all users should be required to enable 2FA when joining the group")
        end

        if @group["default_branch_protection"] < 2
          findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:MEDIUM],
                                                          "Default branch protection for #{@group.name} dooes not prevent merging to main branch", "Setting an appropriate value for default branch protection ensures new repositories are protected from all users writing to the default branch")
        end

        # checks that reduce the security of private and internal projects
        if @group["visibility"] != "Public"
          if !!@group["prevent_sharing_groups_outside_hierarchy"] != true
            findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:MEDIUM],
                                                            "Sharing with groups outside the root group is allowed for group #{@group.name} (non-public group)", "Projects or groups should be shared only with groups inside the existing heirarchy to control access")
          end

          if !!@group["prevent_forking_outside_group"] != true
            findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:LOW],
                                                            "Forking with outside groups is not disabled for group #{@group.name} (non-public group)", "Preventing forking with outside groups makes it more difficult for group members to copy repositories to their personal accounts or share with other projects ")
          end

        else 
          findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:INFORMATIONAL],
          "Visibility for group #{@group.name} is public", "Public groups allow unauthenticated users to access all projects and resources by default")
        end

        findings

      end

      def output_result; end
      def output_statistics; end
    end
  end
end
