# frozen_string_literal: true

require "gitlab_checks/findings"

module GitlabChecks
  module Checks
    class Members
      LAST_GITLAB_ACTIVITY_THRESHOLD_DAYS = 90
      LAST_GITLAB_LOGON_THRESHOLD_DAYS = 60

      def initialize
        @all_members = nil
        @billable_members = nil
        @members_last_activity_exceeded = nil
        @members_last_logon_exceeded = nil
      end

      def audit(gitlab_org)
        findings = []

        @all_members = Gitlab.all_group_members(gitlab_org.root_group, { per_page: 1000 }).lazy_paginate
        @billable_members = Gitlab.group_billable_members(gitlab_org.root_group,
                                                          { per_page: 1000,
                                                            sort: "last_activity_on_asc" }).lazy_paginate

        @members_last_activity_exceeded = @billable_members.select do |m|
          m["last_activity_on"].nil? ||
            Date.parse(m["last_activity_on"]) < (Date.today - LAST_GITLAB_ACTIVITY_THRESHOLD_DAYS)
        end

        if @members_last_activity_exceeded.count.positive?
          findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:MEDIUM],
            "#{@members_last_activity_exceeded.count} users have had no activity within #{LAST_GITLAB_ACTIVITY_THRESHOLD_DAYS} days", "Users who have not performed recent activity on the organisation may no longer be involved with the project and continued access should be validated (see output file)")
        end

        @members_last_logon_exceeded = @billable_members.select do |m|
          m["last_login_at"].nil? ||
            Date.parse(m["last_login_at"]) < (Date.today - LAST_GITLAB_LOGON_THRESHOLD_DAYS)
        end

        if @members_last_logon_exceeded.count.positive?
          findings << GitlabChecks::Findings::Finding.new(GitlabChecks::Findings::SEVERITY[:MEDIUM],
            "#{@members_last_logon_exceeded.count} users have not logged into Gitlab within #{LAST_GITLAB_LOGON_THRESHOLD_DAYS} days", "Users who have not performed recent activity on the organisation may no longer be involved with the project and continued access should be validated (see output file)")
        end
        findings
      end

      def output_result
        headers = %w[username name created_at last_activity_at last_login_at]
        CSV.open("users-no-recent-activity.csv", "w") do |csv|
          csv << headers
          @members_last_activity_exceeded.each do |member_exceeded|
            csv << [member_exceeded.username, member_exceeded.name, member_exceeded.created_at, member_exceeded.last_login_at, member_exceeded.last_activity_on]
          end
        end

        print "Wrote users-no-recent-activity.csv..."

        headers = %w[username name created_at last_activity_at last_login_at]
        CSV.open("users-no-recent-login.csv", "w") do |csv|
          csv << headers
          @members_last_logon_exceeded.each do |member_exceeded|
            csv << [member_exceeded.username, member_exceeded.name, member_exceeded.created_at, member_exceeded.last_login_at, member_exceeded.last_activity_on]
           end
        end

        print "Wrote users-no-recent-login.csv..."
      end

      def output_statistics
        print "Number of root group members: #{@all_members.count}"
        print "Number of billable members: #{@billable_members.count}"
      end

      private

      def print(message)
        $stdout.puts message
      end
    end
  end
end
