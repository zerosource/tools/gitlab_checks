# frozen_string_literal: true

module GitlabChecks
  module Findings

    SEVERITY = { CRITICAL: 1, HIGH: 2, MEDIUM: 3, LOW: 4, INFORMATIONAL: 5 }.freeze

    class Finding
      def initialize(_severity, _title, _description)
        @severity = _severity
        @title = _title
        @description = _description
      end

      def severity_to_string(severity)
        SEVERITY.key(severity)
      end

      def print_console
        puts "(#{severity_to_string(@severity)}) - #{@title}"
        puts "    #{@description}"
        puts ""
      end

      attr_reader :severity, :title, :description

    end
  end
end
