# frozen_string_literal: true

require "optparse"
require "gitlab_checks/checks"
require "gitlab_checks/findings"

module GitlabChecks
  module CLI
    class Application
      STATUS_SUCCESS = 0
      STATUS_ERROR   = 1

      def initialize
        @group_checks = GitlabChecks::Checks::Group.new
        @member_checks = GitlabChecks::Checks::Members.new
        @repo_checks = GitlabChecks::Checks::Repository.new

        @findings = []

        @options = {}
        OptionParser.new do |opts|
          opts.banner = "Usage: gitlab_checks --url URL --group GROUPID --token TOKEN"

          opts.on("-u", "--url URL", "Url of Gitlab API") do |u|
            @options[:endpoint] = u
          end

          opts.on("-g", "--group GROUPID", "ID of the root group for processing") do |u|
            @options[:group] = u
          end

          opts.on("-t", "--token TOKEN", "GitLab private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']") do |u|
            @options[:token] = u
          end

          opts.on("-h", "--help", "Display this screen") do
            puts opts
            exit
          end
        end.parse!
      end

      def execute
        if @options[:endpoint].nil? || @options[:group].nil?
          print("Usage: gitlab_checks --url URL --group GROUPID --token TOKEN")
          exit(STATUS_ERROR)
        end

        if @options[:token].nil? && ENV['GITLAB_API_PRIVATE_TOKEN'].nil?
          print("No GitLab token specified, please use --token TOKEN or set env variable GITLAB_API_PRIVATE_TOKEN")
          exit(STATUS_ERROR)
        end

        print "-----------------------------------"
        print "GITLAB CHECKS v#{GitlabChecks::VERSION} by Zerosource (https://zerosource.io)"
        print "-----------------------------------"
        print "Running checks on GitLab group: #{@options[:group]}"
        print ""

        gitlab_org = GitlabChecks::Connector::Organisation.new(@options[:endpoint], @options[:token], @options[:group])
        do_audit(gitlab_org)
        do_stats(gitlab_org)
        do_output(gitlab_org)
        STATUS_SUCCESS
      end

      def do_audit(gitlab_org)
        @findings.concat(@group_checks.audit(gitlab_org))
        @findings.concat(@member_checks.audit(gitlab_org) || [])
        @findings.concat(@repo_checks.audit(gitlab_org) || [])

        @findings.sort_by!(&:severity)
        print "-----------------------------------"
        print "FINDINGS"
        print "-----------------------------------"
        print ""
        @findings.each(&:print_console)
      end

      def do_stats(_gitlab_org)
        print "-----------------------------------"
        print "STATISTICS"
        print "-----------------------------------"
        print ""
        @group_checks.output_statistics
        @repo_checks.output_statistics
        @member_checks.output_statistics
        print ""
      end

      def do_output(gitlab_org)
        print "-----------------------------------"
        print "OUTPUT"
        print "-----------------------------------"
        print ""
        @group_checks.output_result
        @member_checks.output_result
        @repo_checks.output_result
        print ""
      end

      private

      def print(message)
        $stdout.puts message
      end
    end
  end
end
