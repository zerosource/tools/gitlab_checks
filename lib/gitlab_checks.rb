# frozen_string_literal: true

module GitlabChecks
  class Error < StandardError; end
end

require "gitlab"
require "gitlab_checks/cli"
require "gitlab_checks/checks"
require "gitlab_checks/connector"
require "gitlab_checks/findings"
require "gitlab_checks/version"
